package fm.android.data.remote

import fm.android.data.dto.StreamDto
import retrofit2.http.GET

interface ServerApi {

    @GET("streams.json")
    suspend fun getStreams() : List<StreamDto>

}