package fm.android.data.remote.gson

import com.google.gson.TypeAdapter
import com.google.gson.stream.JsonReader
import com.google.gson.stream.JsonToken
import com.google.gson.stream.JsonWriter
import fm.android.domain.models.enums.StreamStatus

class StreamStatusAdapter : TypeAdapter<StreamStatus>() {

    override fun write(out: JsonWriter?, value: StreamStatus?) {
        out ?: return
        value ?: return
        out.value(value.name.lowercase())
    }

    override fun read(reader: JsonReader?): StreamStatus {
        reader ?: return StreamStatus.TIMEOUT
        if(reader.peek() == JsonToken.NULL) {
            reader.nextNull()
            return StreamStatus.ERROR
        }
        return StreamStatus.find(reader.nextString())
    }

}