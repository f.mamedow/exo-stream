package fm.android.data.repository

import fm.android.data.dto.toDomain
import fm.android.data.remote.ServerApi
import fm.android.data.repository.base.BaseRepository
import fm.android.domain.models.Stream
import fm.android.domain.repository.StreamsRepository
import javax.inject.Inject

class StreamsRepositoryImpl @Inject constructor(
    private val serverApi: ServerApi
): StreamsRepository, BaseRepository() {

    override suspend fun getStreams(): List<Stream> = doRequest{
        val streams = serverApi.getStreams()
        streams.map { it.toDomain() }
    }

}