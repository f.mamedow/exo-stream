package fm.android.data.repository.base

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

open class BaseRepository {

    protected suspend fun <T> doRequest(request: suspend () -> T): T = withContext(Dispatchers.IO) {
        return@withContext request()
    }

}