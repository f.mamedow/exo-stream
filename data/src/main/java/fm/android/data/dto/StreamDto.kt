package fm.android.data.dto

import com.google.gson.annotations.SerializedName
import fm.android.domain.models.Stream
import fm.android.domain.models.enums.StreamStatus

data class StreamDto(
    @SerializedName("channel")
    val channelName: String?,
    @SerializedName("url")
    val url: String,
    @SerializedName("http_referrer")
    val webSite: String?,
    @SerializedName("user_agent")
    val userAgent: String?,
    @SerializedName("status")
    val status: StreamStatus,
    @SerializedName("width")
    val width: Int?,
    @SerializedName("height")
    val height: Int?,
    @SerializedName("bitrate")
    val bitrate: Int?,
    @SerializedName("frame_rate")
    val frameRate: Float?,
    @SerializedName("added_at")
    val addedAt: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("checked_at")
    val checked_at: String,
)


fun StreamDto.toDomain() = Stream(
    channelName = channelName ?: "Unknown Channel",
    url = url,
    webSite = webSite,
    userAgent = userAgent,
    status = status,
    width = width,
    height = height,
    bitrate = bitrate,
    frameRate = frameRate,
    addedAt = addedAt,
    updatedAt = updatedAt,
    checked_at = checked_at
)