package fm.android.data.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import fm.android.data.remote.ServerApi
import fm.android.data.remote.gson.StreamStatusAdapter
import fm.android.data.repository.StreamsRepositoryImpl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import fm.android.domain.Constants
import fm.android.domain.models.enums.StreamStatus
import fm.android.domain.repository.StreamsRepository
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module(
    includes = [DataModule.Binder::class]
)
@InstallIn(ViewModelComponent::class)
object DataModule {

    @Provides
    fun provideGson(): Gson {
        return GsonBuilder()
            .registerTypeAdapter(StreamStatus::class.java, StreamStatusAdapter())
            .create()
    }

    @Provides
    fun provideOkHttpClient() : OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    fun provideServerApi(retrofit: Retrofit) : ServerApi {
        return retrofit.create(ServerApi::class.java)
    }

    @Module
    @InstallIn(ViewModelComponent::class)
    interface Binder {
        @Binds
        fun bindStreamsRepository(streamsRepositoryImpl: StreamsRepositoryImpl) : StreamsRepository
    }
}