package fm.android.exostream.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.withStarted
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import fm.android.exostream.R
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

abstract class BaseFragment<VB : ViewBinding>(
    private val bindingInflater: (layoutInflater: LayoutInflater) -> VB,
) : Fragment() {

    protected var binding: VB? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = bindingInflater(inflater)
        this.binding = binding
        return binding.root
    }

    @LayoutRes
    abstract fun getLayoutRes(): Int

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    protected fun <T> StateFlow<T>.collectUIState(
        lifecycleState: Lifecycle.State = Lifecycle.State.STARTED,
        action: (T) -> Unit
    ) {
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(lifecycleState) {
                this@collectUIState.collect {
                    action(it)
                }
            }
        }
    }


    protected fun getCurrentFragment() : Fragment? {
        return requireActivity().supportFragmentManager.primaryNavigationFragment
            ?.childFragmentManager
            ?.fragments
            ?.last()
    }

}