package fm.android.exostream.ui.fragments.home

import fm.android.exostream.ui.models.StreamUI

data class HomeUiState(
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val streams: List<StreamUI>? = null,
)