package fm.android.exostream.ui.fragments.stream

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import com.google.android.exoplayer2.C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import fm.android.exostream.R
import fm.android.exostream.databinding.FragmentStreamBinding
import fm.android.exostream.ui.fragments.BaseFragment

class StreamFragment : BaseFragment<FragmentStreamBinding>(FragmentStreamBinding::inflate) {

    private val args : StreamFragmentArgs by navArgs()

    private var exoPlayer : ExoPlayer? = null


    override fun getLayoutRes() = R.layout.fragment_stream

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupView()
    }

    private fun setupView() {
        if (exoPlayer == null) {

            exoPlayer = ExoPlayer.Builder(requireContext())
                .setVideoScalingMode(VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
                .build()

            val mediaItem = MediaItem.Builder()
                .setUri(args.streamUrl)
                .build()

            val defaultDataSourceFactory = DefaultHttpDataSource.Factory()
            val hlsMediaSource = HlsMediaSource.Factory(defaultDataSourceFactory)
                .createMediaSource(mediaItem)

            binding?.playerView?.player = exoPlayer
            binding?.playerView?.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_WIDTH

            exoPlayer?.setMediaSource(hlsMediaSource)
            exoPlayer?.prepare()
            exoPlayer?.play()


            binding?.tvName?.text = args.channelName
        }
    }

    override fun onPause() {
        requireActivity().requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        exoPlayer?.pause()
        exoPlayer?.release()
        super.onPause()
    }

}