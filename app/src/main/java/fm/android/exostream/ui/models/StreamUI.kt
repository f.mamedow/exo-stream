package fm.android.exostream.ui.models

import android.os.Parcelable
import fm.android.domain.models.Stream
import fm.android.domain.models.enums.StreamStatus
import kotlinx.parcelize.Parcelize

@Parcelize
data class StreamUI(
    val channelName: String,
    val url: String,
    val webSite: String?,
    val userAgent: String?,
    val status: StreamStatus,
    val width: Int?,
    val height: Int?,
    val bitrate: Int?,
    val frameRate: Float?,
    val addedAt: String,
    val updatedAt: String,
    val checked_at: String,
) : Parcelable



fun Stream.toUI() = StreamUI(
    channelName = channelName,
    url = url,
    webSite = webSite,
    userAgent = userAgent,
    status = status,
    width = width,
    height = height,
    bitrate = bitrate,
    frameRate = frameRate,
    addedAt = addedAt,
    updatedAt = updatedAt,
    checked_at = checked_at
)