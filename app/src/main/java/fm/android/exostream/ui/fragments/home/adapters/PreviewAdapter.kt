package fm.android.exostream.ui.fragments.home.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import fm.android.exostream.ui.fragments.stream_preview.StreamPreviewFragment
import fm.android.exostream.ui.models.StreamUI

class PreviewAdapter(
    activity: FragmentActivity,
    private val streams: List<StreamUI>
) : FragmentStateAdapter(activity) {

    override fun getItemCount(): Int = streams.size

    override fun createFragment(position: Int): Fragment {
        val stream = streams[position]
        return StreamPreviewFragment.newInstance(stream)
    }

}