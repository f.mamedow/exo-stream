package fm.android.exostream.ui.fragments.stream_preview

import android.os.Bundle
import android.transition.TransitionManager
import android.util.Log
import android.view.View
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.savedstate.SavedStateRegistry
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.source.hls.HlsMediaSource
import com.google.android.exoplayer2.ui.AspectRatioFrameLayout
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import fm.android.domain.models.Stream
import fm.android.domain.models.enums.StreamStatus
import fm.android.exostream.R
import fm.android.exostream.databinding.FragmentStreamPreviewBinding
import fm.android.exostream.ui.fragments.BaseFragment
import fm.android.exostream.ui.fragments.home.HomeFragment
import fm.android.exostream.ui.fragments.home.HomeFragmentDirections
import fm.android.exostream.ui.models.StreamUI

class StreamPreviewFragment :
    BaseFragment<FragmentStreamPreviewBinding>(FragmentStreamPreviewBinding::inflate) {

    private var exoPlayer: ExoPlayer? = null

    override fun getLayoutRes() = R.layout.fragment_stream_preview

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val stream: StreamUI? = requireArguments().getParcelable(FRAGMENT_ARG_STREAM)

        stream?.let {
            setupInfo(it)
            setupListeners(it)
            setupPlayer(it)

            toggleInfoCard()
        }
    }

    private fun setupListeners(stream: StreamUI) {
        binding?.ivInfo?.setOnClickListener {
            toggleInfoCard()
        }

        binding?.ivFullscreen?.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeToStream(
                streamUrl = stream.url,
                channelName = stream.channelName,
            )
            findNavController().navigate(action)
            stopPlayer()
        }
    }

    private fun toggleInfoCard() {
        binding?.let { binding ->

            TransitionManager.beginDelayedTransition(binding.root)
            if (binding.infoCard.tag == true) {
                val constraintSet = ConstraintSet()
                constraintSet.clone(binding.root)

                constraintSet.clear(binding.infoCard.id, ConstraintSet.TOP)
                constraintSet.clear(binding.infoCard.id, ConstraintSet.BOTTOM)

                constraintSet.connect(binding.infoCard.id, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
                constraintSet.applyTo(binding.root)

            } else {
                val constraintSet = ConstraintSet()
                constraintSet.clone(binding.root)

                constraintSet.clear(binding.infoCard.id, ConstraintSet.TOP)
                constraintSet.clear(binding.infoCard.id, ConstraintSet.BOTTOM)

                constraintSet.connect(binding.infoCard.id, ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
                constraintSet.applyTo(binding.root)
            }
            binding.infoCard.tag = binding.infoCard.tag != true
        }
    }

    private fun setupInfo(stream: StreamUI) {

        binding?.tvChannelName?.text = stream.channelName
        binding?.tvStatusValue?.text = stream.status.name.lowercase()

        val statusDrawableRes = when (stream.status) {
            StreamStatus.ONLINE -> R.drawable.status_shape_online
            StreamStatus.BLOCKED -> R.drawable.status_shape_error
            StreamStatus.TIMEOUT -> R.drawable.status_shape_timeout
            StreamStatus.ERROR -> R.drawable.status_shape_error
        }
        binding?.ivStatus?.setImageResource(statusDrawableRes)

        if (stream.width == null && stream.height == null) {
            binding?.tvSizeLabel?.isVisible = false
            binding?.tvSizeValue?.isVisible = false
        } else {
            binding?.tvSizeLabel?.isVisible = true
            binding?.tvSizeValue?.isVisible = true
            binding?.tvSizeValue?.text = String.format("%dx%d", stream.width, stream.height)
        }

        if (stream.bitrate == null || stream.bitrate == 0) {
            binding?.tvBitrateLabel?.isVisible = false
            binding?.tvBitrateValue?.isVisible = false
        } else {
            binding?.tvBitrateLabel?.isVisible = true
            binding?.tvBitrateValue?.isVisible = true
            binding?.tvBitrateValue?.text = stream.bitrate.toString()
        }

        if (stream.frameRate == null || stream.frameRate == 0f) {
            binding?.tvFrameRateLabel?.isVisible = false
            binding?.tvFrameRateValue?.isVisible = false
        } else {
            binding?.tvFrameRateLabel?.isVisible = false
            binding?.tvFrameRateValue?.isVisible = false
            binding?.tvFrameRateValue?.text = stream.frameRate.toString()
        }

        if (stream.webSite == null) {
            binding?.tvWebsiteLabel?.isVisible = false
            binding?.tvWebsiteValue?.isVisible = false
        } else {
            binding?.tvWebsiteLabel?.isVisible = false
            binding?.tvWebsiteValue?.isVisible = false
            binding?.tvWebsiteValue?.text = stream.webSite
        }

    }

    private fun setupPlayer(stream: StreamUI) {
        if (isCurrentFragmentHome().not()) return
        exoPlayer = ExoPlayer.Builder(requireContext())
            .setVideoScalingMode(C.VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING)
            .build()

        val mediaItem = MediaItem.Builder()
            .setUri(stream.url)
            .build()

        val defaultHttpSourceFactory = DefaultHttpDataSource.Factory()

        val hlsDataSource = HlsMediaSource.Factory(defaultHttpSourceFactory)
            .createMediaSource(mediaItem)

        binding?.previewPlayer?.player = exoPlayer
        binding?.previewPlayer?.resizeMode = AspectRatioFrameLayout.RESIZE_MODE_FIXED_HEIGHT

        exoPlayer?.setMediaSource(hlsDataSource)

        exoPlayer?.prepare()
        exoPlayer?.play()
    }

    private fun stopPlayer() {
        exoPlayer?.stop()
        exoPlayer?.release()
    }

    override fun onPause() {
        stopPlayer()
        super.onPause()
    }

    override fun onDestroy() {
        exoPlayer = null
        super.onDestroy()
    }

    private fun isCurrentFragmentHome(): Boolean {
        return getCurrentFragment()?.javaClass?.name == HomeFragment::class.java.name
    }

    companion object {
        fun newInstance(stream: StreamUI) : StreamPreviewFragment {
            val fragment = StreamPreviewFragment()
            fragment.arguments = bundleOf(
                FRAGMENT_ARG_STREAM to stream
            )
            return fragment
        }

        private const val FRAGMENT_ARG_STREAM = "stream_arg"
    }

}