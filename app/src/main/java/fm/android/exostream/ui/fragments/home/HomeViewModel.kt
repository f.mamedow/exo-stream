package fm.android.exostream.ui.fragments.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import fm.android.domain.Resource
import fm.android.domain.usecase.GetStreamsUseCase
import fm.android.exostream.ui.models.toUI
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getStreamsUseCase: GetStreamsUseCase,
) : ViewModel() {

    private val _uiState = MutableStateFlow(HomeUiState())
    val uiState = _uiState.asStateFlow()

    init {
        loadStreams()
    }

    private fun loadStreams() = viewModelScope.launch {
        getStreamsUseCase()
            .collectLatest { resource ->
                when (resource) {
                    is Resource.Error -> {
                        _uiState.emit(HomeUiState(isError = true))
                    }
                    is Resource.Loading -> {
                        _uiState.emit(HomeUiState(isLoading = true))
                    }
                    is Resource.Success -> {
                        val streams = resource.data.streams.map { it.toUI() }
                        _uiState.emit(HomeUiState(streams = streams))
                    }
                }
            }
    }


}