package fm.android.exostream.ui.fragments.home

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import fm.android.domain.models.Stream
import fm.android.exostream.R
import fm.android.exostream.databinding.FragmentHomeBinding
import fm.android.exostream.ui.fragments.BaseFragment
import fm.android.exostream.ui.fragments.home.adapters.PreviewAdapter
import fm.android.exostream.ui.models.StreamUI

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    private val viewModel: HomeViewModel by viewModels()

    override fun getLayoutRes() = R.layout.fragment_home

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservers()
    }

    private fun setupObservers() {
        viewModel.uiState.collectUIState { state ->
            binding?.loader?.isVisible = state.isLoading
            if (state.isError) {
                Toast.makeText(requireContext(), getString(R.string.connection_error), Toast.LENGTH_SHORT).show()
            }
            state.streams?.let {
                setStreamPreview(it)
            }
        }
    }

    private fun setStreamPreview(streams: List<StreamUI>) {
        binding?.vpStreams?.adapter = PreviewAdapter(requireActivity(), streams)
    }

}