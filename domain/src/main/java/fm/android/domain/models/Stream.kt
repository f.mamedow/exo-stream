package fm.android.domain.models

import fm.android.domain.models.enums.StreamStatus

data class Stream(
    val channelName: String,
    val url: String,
    val webSite: String?,
    val userAgent: String?,
    val status: StreamStatus,
    val width: Int?,
    val height: Int?,
    val bitrate: Int?,
    val frameRate: Float?,
    val addedAt: String,
    val updatedAt: String,
    val checked_at: String,
)
