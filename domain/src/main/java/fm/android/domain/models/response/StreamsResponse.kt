package fm.android.domain.models.response

import fm.android.domain.models.Stream

data class StreamsResponse(
    val streams: List<Stream>
)