package fm.android.domain.models.enums

enum class StreamStatus() {
    ONLINE, BLOCKED, TIMEOUT, ERROR;

    companion object{
        fun find(name: String) = values().find { name == it.name.lowercase() } ?: ERROR
    }

}