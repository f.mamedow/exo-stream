package fm.android.domain.usecase

import fm.android.domain.Resource
import fm.android.domain.models.response.StreamsResponse
import fm.android.domain.repository.StreamsRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetStreamsUseCase @Inject constructor(
    private val streamsRepository: StreamsRepository,
) {

    suspend operator fun invoke() : Flow<Resource<StreamsResponse>> = flow {
        emit(Resource.Loading())
        try {
            val streams = streamsRepository.getStreams().sortedBy {
                it.status.ordinal
            }
            val response = StreamsResponse(streams)
            emit(Resource.Success(response))
        } catch (e: Exception) {
            emit(Resource.Error(e.message))
        }

    }

}