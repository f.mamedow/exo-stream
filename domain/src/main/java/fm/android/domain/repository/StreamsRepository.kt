package fm.android.domain.repository

import fm.android.domain.models.Stream

interface StreamsRepository {

    suspend fun getStreams() : List<Stream>

}